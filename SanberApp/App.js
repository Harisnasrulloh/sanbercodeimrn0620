import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import YoutubeUI from './Tugas/Tugas12/App'

import LoginPage from './Tugas/Tugas13/LoginScreen'

import AboutProfile from './Tugas/Tugas13/AboutScreen'

import Tugas14 from './Tugas/Tugas14/App';

import SkillScreen from './Tugas/Tugas14/SkillScreen'

import Tugas15no1 from './Tugas/Tugas15/index';

import Tugas15no2 from './Tugas/TugasNavigation/index';

import Quiz3 from './Quiz3/index';

export default function App() {
  return (
    // <YoutubeUI/>

    // <LoginPage/>
    // <AboutProfile/>
    
    // <Tugas14/>
    // <SkillScreen/>

    // <Tugas15no1/>
    // <Tugas15no2/>
    <Quiz3/>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#3883fc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  Text : {
    color : '#03ff0b'
  }
});
