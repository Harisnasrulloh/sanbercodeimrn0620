import React from "react";
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator();
const Tambah = () => {
    return(
        <View style={styles.container}>
            <Text>Halaman Tambah</Text>
        </View>
    )
}

export const Add = ()=>{
  return(
    <Stack.Navigator>
        <Stack.Screen name="Add" component={Tambah}/>
    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})