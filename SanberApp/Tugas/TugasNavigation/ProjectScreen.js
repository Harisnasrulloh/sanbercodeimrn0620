import React from "react";
import { StyleSheet, Text, View } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack'

const Stack = createStackNavigator();

const proyek = () => {
    return(
        <View style={styles.container}>
            <Text>Halaman Proyek</Text>
        </View>
    )
}

export const Project = ()=>{
  return(
    <Stack.Navigator>
        <Stack.Screen name="project" component={proyek}/>
    </Stack.Navigator>
  )
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})