import React, {Component} from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import VideoItem from "./components/videoItem";
import data from "./data.json";

export default class App extends Component {
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.navBar}>
                    <Image source={require('./logo.png')} style = {{width:100,height:22}}></Image>
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name='search' size ={25}/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Icon style={styles.navItem} name='account-circle' size = {25} />
                        </TouchableOpacity>                    
                    </View>
                </View>
                <View style={styles.body}>
                    <FlatList
                    data = {data.items}
                    renderItem = {(video)=><VideoItem video={video.item}/>}
                    keyExtractor = {(item) => item.id}
                    ItemSeparatorComponent = {()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
                    />
                </View>
                <View style={styles.tapBar}>
                    <TouchableOpacity style={styles.tapItem}>
                        <Icon name='home' size={25}/>
                        <Text style={styles.tapTittle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tapItem}>
                        <Icon name='whatshot' size={25}/>
                        <Text style={styles.tapTittle}>Trending</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tapItem}>
                        <Icon name='subscriptions' size={25}/>
                        <Text style={styles.tapTittle}>Subscriptions</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tapItem}>
                        <Icon name='folder' size={25}/>
                        <Text style={styles.tapTittle}>Library</Text>
                    </TouchableOpacity>

                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
    },
    navBar: {
        height: 75,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav: {
        flexDirection: 'row'
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex:1
    },
    tapBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tapItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tapTittle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 3
    }
})