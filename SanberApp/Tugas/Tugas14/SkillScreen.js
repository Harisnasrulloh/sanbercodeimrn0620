import React from "react";
import {
	View,
	Image,
  Text,
  ScrollView,
  TouchableOpacity,
	StyleSheet,
	FlatList
} from "react-native";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import data from './skillData.json';
import SkillItem from './components/skillItem';


export default class SkillScreen extends React.Component {
	render() {
    return (
			<View style={styles.container}>
				<Image source={require('./../Tugas13/image/logo.png')} style={styles.image}/>
				<View style={styles.profile}>
					<Icon name='account-circle' size={50} color={'#3EC6FF'}/>
					<View style={styles.detailProfile}>
						<Text>hai,</Text>
						<Text>Haris Nasrulloh</Text>
					</View>
				</View>
				<Text style={styles.skillText}>Skill</Text>
				<View style={{height: 4, backgroundColor:'#3EC6FF'}}/>

				<View style={styles.listPortofolio}>
					<TouchableOpacity style={styles.itemPortofolio}>
						<Text style={styles.itemPortofolioText}>Library / Framework</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.itemPortofolio}>
						<Text style={styles.itemPortofolioText}>Bahasa Pemrograman</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.itemPortofolio}>
						<Text style={styles.itemPortofolioText}>Teknologi</Text>
					</TouchableOpacity>
				</View>

				<FlatList
					data = {data.items}
					renderItem = {(skill)=><SkillItem skill={skill.item}/>}
					keyExtractor = {(item) => item.id}
					ItemSeparatorComponent = {()=><View style={{height:0.5, backgroundColor:'#E5E5E5'}}/>}
					/>
			</View>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
		marginHorizontal: 20
	},
	image: {
		width: 250,
		height: 75,
		alignSelf: 'flex-end'
	},
	profile: {
		flexDirection: 'row',
		marginRight: 10
	},
	detailProfile: {
		flexDirection: 'column',
		margin: 5,
		color: '#003366'
	},
	skillText: {
		fontSize: 40,
		color: '#003366',
		fontWeight: 'bold'
	},
	listPortofolio: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginVertical: 10
	},
	itemPortofolio: {
		backgroundColor: '#B4E9FF',
		borderRadius: 8,
		height: 40,
		padding: 10
	},
	itemPortofolioText: {
		fontSize: 12,
		color: '#003366',
		fontWeight: 'bold'
	}
})