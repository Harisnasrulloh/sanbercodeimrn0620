import React, {Component} from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";


export default class skillItem extends Component {
	render(){
		let skill = this.props.skill
		return (
			<View style={styles.container}>
				<Icon name= {skill.iconName} size={75} style={styles.logo}/>
				<View style={styles.detailSkill}>
					<Text style={styles.titleText}>{skill.skillName}</Text>
					<Text style={styles.typeText}>{skill.category}</Text>
					<Text style={styles.percentage}>{skill.percentageProgress}</Text>
				</View>
				<Icon name='chevron-right' color='#003366' size={75}/>
			</View>
		)
	} 
}

const styles = StyleSheet.create({
	
	container: {
		backgroundColor: '#B4E9FF',
		borderRadius: 8,
		flexDirection: 'row',
		marginBottom: 10,
		padding: 10,
		justifyContent: 'space-around'
	},
	logo: {
		color: '#003366'
	},
	detailSkill: {
		flexDirection: 'column',
		alignItems: 'flex-start'
	},
	titleText: {
		color: '#003366',
		fontSize: 20,
		fontWeight: 'bold'
	},
	typeText: {
		fontSize: 16,
		color: '#3EC6FF',
		fontWeight: 'bold'
	},
	percentage: {
		fontSize: 30,
		color: '#FFFFFF',
		fontWeight: 'bold',
		marginLeft: 75
	},
})