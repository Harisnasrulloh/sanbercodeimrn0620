console.log("Nomor 1")
console.log("LOOPING PERTAMA")
var i = 2
while (i<=20) {
    console.log(i+" - I love coding")
    i+=2
}

console.log("LOOPING KEDUA")
var j = 20
while (j>=2) {
    console.log(j+" - I will become a mobile developer")
    j-=2
}
console.log("")

console.log("Nomor 2")
for (let i = 1; i <=20; i++) {
    if (i%2 == 1) {
        if (i%3 == 0) {
            console.log(i+" - I Love Coding")
        } else {
            console.log(i+" - Santai")
        }
    } else {
        console.log(i+" - Berkualitas")
    }
}
console.log("")

console.log("Nomor 3")
var output3 = ''
for (let i = 0; i < 4; i++) {
    output3 = ''
    for (let j = 0; j < 8; j++) {
        output3 += '#'
    }
    console.log(output3);
}
console.log("")


console.log("Nomor 4")
var output4 = ''
for (let i = 0; i < 7; i++) {
    output4 = ''
    for (let j = 0; j <= i; j++) {
        output4 += '#'
    }
    console.log(output4);
}
console.log("")

console.log("Nomor 5")
var output5 = ''
for (let i = 0; i < 8; i++) {
    output5 = ''
    for (let j = 0; j < 8; j++) {
        if (i%2 == 1 && j%2 ==0) {
            output5 += '#'
        } else if (i%2 == 0 && j%2 ==1) {
            output5 += '#'
        } else {
            output5 += ' '
        }
    }
    console.log(output5);
}