console.log('Nomor 1');
// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var time = 10000

// Tulis code untuk memanggil function readBooks di sini
readBooks(time,books[0],function(time) {
	readBooks(time,books[1],function(time) {
		readBooks(time,books[2],function(time) {
			console.log("Selesai baca buku")
		})
	})
})