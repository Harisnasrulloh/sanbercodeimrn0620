console.log('Nomor 2');

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
var time = 10000

// Lanjutkan code untuk menjalankan function readBooksPromise 
function read(time) {
    readBooksPromise(time,books[0])
        .then(function(time){
            readBooksPromise(time,books[1])
                .then(function(time){
                    readBooksPromise(time,books[2])
                        .then(function(time){
                            console.log('Selesai membaca Semua buku');
                        })
                        .catch(function(time){
                            console.log('Waktu saya baca '+books[2].name+' : '+((books[2].timeSpent+ time)/1000)+' detik');
                        })
                })
                .catch(function(time){
                    console.log('Waktu saya baca '+books[1].name+' : '+((books[1].timeSpent+ time)/1000)+' detik');
                })
        })
        .catch(function(time){
            console.log('Waktu saya baca '+books[0].name+' : '+((books[0].timeSpent+ time)/1000)+' detik');
        })
}

read(time)