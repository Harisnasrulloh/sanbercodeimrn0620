console.log("Nomor 1");

function range(startNum, finishNum) {
    if (startNum == null || finishNum == null) {
        return -1
    } else {
        var list = []
        if (startNum > finishNum) {
            for (let i = startNum; i >= finishNum; i--) {
                list.push(i)                
            }
        } else {
            for (let i = startNum; i <= finishNum; i++) {
                list.push(i)                
            }
        }
        return list
    }
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18))
console.log(range(54, 50))
console.log(range())

console.log();

console.log("Nomor 2");

function rangeWithStep(startNum, finishNum, step) {
    if (startNum == null || finishNum == null) {
        return -1
    } else {
        var list = []
        if (startNum > finishNum) {
            for (let i = startNum; i >= finishNum; i-= step) {
                list.push(i)                
            }
        } else {
            for (let i = startNum; i <= finishNum; i+= step) {
                list.push(i)                
            }
        }
        return list
    }
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

console.log();

console.log("Nomor 3");

function sum(startNum, finishNum, step) {
    var add = 0
    if (startNum == null) {
        return add
    }
    else if (finishNum == null) {
        return startNum
    } else {
        if (step == null) {
            step = 1
        }
        if (startNum > finishNum) {
            for (let i = startNum; i >= finishNum; i-= step) {
                add = add + i
            }
        } else {
            for (let i = startNum; i <= finishNum; i+= step) {
                add = add + i
            }
        }
        return add
    }
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

console.log();

console.log("Nomor 4");

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling (input) {
    for (let i = 0; i < input.length ; i++) {
        console.log('Nomor ID:  '+input[i][0]);
        console.log('Nama Lengkap:  '+input[i][1]);
        console.log('TTL:  '+input[i][2]+' '+input[i][3]);
        console.log('Hobi:  '+input[i][4]);
        console.log();
    }
}
dataHandling (input)

console.log();

console.log("Nomor 5");

function balikKata(input) {
    var balik = ''
    for (let i = input.length-1; i >= 0; i--) {
        balik = balik.concat((input[i]))
    }
    return balik
}

console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I

console.log();

console.log("Nomor 6");

function dataHandling2(input) {

    input.splice(1,2,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung")
    input.splice(4,1,"Pria","SMA Internasional Metro")
    console.log(input);

    var tanggal = input[3]
    var TL = tanggal.split('/')
    var TL2 = tanggal.split('/')

    var convertBulan
    switch (TL[1]) {
        case '01': {convertBulan = 'Januari ';break;}
        case '02': {convertBulan = 'Februari ';break;}
        case '03': {convertBulan = 'Maret ';break;}
        case '04': {convertBulan = 'April ';break;}
        case '05': {convertBulan = 'Mei ';break;}
        case '06': {convertBulan = 'Juni ';break;}
        case '07': {convertBulan = 'Juli ';break;}
        case '08': {convertBulan = 'Agustus ';break;}
        case '09': {convertBulan = 'September ';break;}
        case '10': {convertBulan = 'Oktober';break;}
        case '11': {convertBulan = 'November';break;}
        case '12': {convertBulan = 'Desember';break;}
        default: {convertBulan = 'Salah bulan lurr';}        
    }
    console.log(convertBulan);

    var urut = TL.sort(function (value1, value2) { return value2 - value1 } )
    console.log(urut)

    var joinTanggal = TL2.join("-")
    console.log(joinTanggal);
    
    var potongNama = input[1].split("")
    potongNama = potongNama.slice(0,15)
    input[1] = potongNama.join("")
    console.log(input[1]);
    
}

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);