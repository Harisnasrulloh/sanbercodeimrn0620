console.log('Nomor 1');
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    if (arr[0] == null) {
        console.log('Tidak ada datanya');
    } else {
        for (let i = 0; i < arr.length; i++) {
            var data = {}
            if (arr[i][3] == null || arr[i][3] > thisYear) {
                arr[i][3] = 'Invalid Birth Year'
            }else {
                arr[i][3] = thisYear - arr[i][3]
            }
            data.firstName = arr[i][0],
            data.lastName= arr[i][1],
            data.gender= arr[i][2],
            data.age = arr[i][3]
            console.log((i+1)+'. '+data.firstName+' '+data.lastName+' : ');
            console.log(data);
        }    
    }
    
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people)
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([])

console.log();

console.log('Nomor 2');

function shoppingTime(memberId, money) {
    if (memberId == null || memberId == '') {
       return 'Mohon maaf, toko X hanya berlaku untuk member saja'
    } else {
        if (money < 50000 ) {
            return 'Mohon maaf, uang tidak cukup'
        } else {
            var barang = [
                {nama : 'Sepatu Stacattu', harga : 1500000},
                {nama : 'Baju Zoro', harga : 500000},
                {nama : 'Baju H&N', harga : 250000},
                {nama : 'Sweater Uniklooh', harga : 175000},
                {nama : 'Casing Handphone', harga : 50000}
            ]
            var hasil2 = {}
            var listPurchased = []
            hasil2.memberId = memberId
            hasil2.money = money
            for (let i = 0; i < barang.length; i++) {
                if (money >= barang[i].harga) {
                    listPurchased.push(barang[i].nama)
                    money -= barang[i].harga
                }
            }
            hasil2.listPurchased = listPurchased
            hasil2.changeMoney = money

            return hasil2
        }
    }
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

console.log();

console.log('Nomor 3');
function naikAngkot(arrPenumpang) {

    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var hasil3 = []
    
    var bayar = 0
    for (let i = 0; i < arrPenumpang.length; i++) {
        var listPenumpang = {}
        listPenumpang.penumpang = arrPenumpang[i][0]
        console.log(listPenumpang.penumpang);
        
        listPenumpang.naikDari = arrPenumpang[i][1]
        listPenumpang.tujuan = arrPenumpang[i][2]
        var bayar = 0
        for (let j = 0; j < rute.length; j++) {
            if (rute[j] > listPenumpang.naikDari  && rute[j] <= listPenumpang.tujuan) {
                bayar+=2000
            }
        }
        listPenumpang.bayar = bayar
        
        hasil3.push(listPenumpang)
    }
    return hasil3
}
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]