console.log("If - Else")
var nama = 'Hanas'
var peran = 'Werewolf'

if (nama == '') {
    console.log("Nama harus diisi!")
} else if (peran == '') {
    console.log("Halo "+nama+", Pilih peranmu untuk memulai game!")
} else {
    console.log("Selamat datang di Dunia Werewolf, "+nama)
    if (peran == 'Penyihir') {
        console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!")
    } else if (peran == 'Guard') {
        console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.")
    } else if (peran == 'Werewolf') {
        console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!")
    }
}
console.log("")

console.log("Switch - Case")
var tanggal = 20
var bulan = 1
var tahun = 2020
var convertBulan

switch (bulan) {
    case 1: {convertBulan = 'Januari ';break;}
    case 2: {convertBulan = 'Februari ';break;}
    case 3: {convertBulan = 'Maret ';break;}
    case 4: {convertBulan = 'April ';break;}
    case 5: {convertBulan = 'Mei ';break;}
    case 6: {convertBulan = 'Juni ';break;}
    case 7: {convertBulan = 'Juli ';break;}
    case 8: {convertBulan = 'Agustus ';break;}
    case 9: {convertBulan = 'September ';break;}
    case 10: {convertBulan = 'Oktober';break;}
    case 11: {convertBulan = 'November';break;}
    case 12: {convertBulan = 'Desember';break;}
    default: {convertBulan = 'Salah bulan lurr';}        
}

console.log(tanggal+' '+convertBulan+' '+tahun)